import Login from "../src/component/Login/Login";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ForgotPassword from "./component/Login/ForgotPassword";
import NewPassword from "./component/Login/NewPassword";
import Dashboard from "./component/Dashboard/Dashboard";
import ProtectedRoute from "./PrivateRoute/ProtectedRoute";
import SideBar from "./component/NavBar_des/SideBar";
import CreateSyllabus from "./component/Syllabus/CreateSyllabus";
import ViewSyllabus from "./component/Syllabus/ViewSyllabus";
import ViewProgram from "./component/TrainingProgram/ViewProgram";
import CreateProgram from "./component/TrainingProgram/CreateProgram";
import ViewClass from "./component/Class/ViewClass";
import CreateClass from "./component/Class/CreateClass";
import TrainingCalendar from "./component/TrainingCalendar/TrainingCalendar";
import UserList from "./component/UserManagement/UserList";
import UserPerm from "./component/UserManagement/UserPerm";
import Materials from "./component/Learning/Materials";
import Calendar from "./component/Setting/Calendar";
import PageNotFound from "./component/PageNotFound";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/forgotPassword" element={<ForgotPassword />}></Route>
        <Route path="/newPassword" element={<NewPassword />}></Route>
        <Route path="/" element={<Login />} />
        <Route element={<ProtectedRoute />}>
          <Route path="/dashboard" element={<Dashboard />}></Route>
          <Route path="/syllabus/create" element={<CreateSyllabus />}></Route>
          <Route path="/syllabus/view" element={<ViewSyllabus />}></Route>
          <Route path="/training/view" element={<ViewProgram />}></Route>
          <Route path="training/create" element={<CreateProgram />}></Route>
          <Route path="class/view" element={<ViewClass />}></Route>
          <Route path="/class/create" element={<CreateClass />}></Route>
          <Route path="/training/calendar" element={<TrainingCalendar />}></Route>
          <Route path="/user/list" element={<UserList />}></Route>
          <Route path="/user/permission" element={<UserPerm />}></Route>
          <Route path="/materials" element={<Materials />}></Route>
          <Route path="/setting/calendar" element={<Calendar />}></Route>
          
        </Route>
        <Route path="*" element={<PageNotFound />} /> 
        {/* <Route path="*" element={<CreateSyllabus />} /> */}
        
      </Routes>
    </BrowserRouter>
  );
}

export default App;
