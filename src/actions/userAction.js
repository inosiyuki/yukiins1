import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { API_URL } from "../api";


const instance = axios.create({
  baseURL: API_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

// instance.interceptors.request.use(
//   (config) => {
//     const token = JSON.stringify(getLocalAccessToken());
//     if(token){
//       config.headers['Bearer ' + token] =  token;
//     }
//     return config;
//   },
//   (err) => {
//     return Promise.reject(err);
//   }
// );

// instance.interceptors.response.use(
//   (response) => {
//     return response;
//   },
//   async (err) => {
//     const originalConfig = err.config

//     if (err.response) {
//       // Access Token was expired
//       if (err.response.status === 401 && !originalConfig._retry) {
//         originalConfig._retry = true;

//         try {
//           const rs = await instance.post('/auth/accesstoken', 
//             JSON.stringify(localStorage.getItem('refreshToken'))
//           );
          
//           localStorage.setItem('accessToken', rs.data.accessToken)
//           return instance(originalConfig)
//         } catch {
//           return await Promise.reject(err);
//         }
//       }

//   // Use a 'clean' instance of axios without the interceptor to refresh the token. No more infinite refresh loop.
//     }
//   }
// );

export const userLogin = createAsyncThunk(
  "user/login",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      // configure header's Content-Type as JSON
      

      const response = await instance.post(
        "auth/login",
        JSON.stringify({ email, password }),
        
      );

      localStorage.setItem("accessToken", response.data.data.accessToken);
      localStorage.setItem("refreshToken", response.data.data.refreshToken);

      // store user's token/information including token in local storage

      return response;
    } catch (error) {
      // return custom error message from API if any
      if (error.response && error.response.data.message) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue(error.message);
      }
    }
  }
);

// export const logout = () => {

// };

export const userLogout = createAsyncThunk("auth/logout", async () => {
  localStorage.removeItem("accessToken");
  localStorage.removeItem("refreshToken");
});
