export const getLocalAccessToken = () => {
    const accessToken = JSON.parse(localStorage.getItem('accessToken'));
    return accessToken?.accessToken
}

export const getLocalRefreshToken = () => {
    const refreshToken = JSON.parse(localStorage.getItem('refreshToken'))
    return refreshToken?.refreshToken
}

