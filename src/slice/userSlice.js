// userSlice.js
import { createSlice } from "@reduxjs/toolkit";

import { userLogin } from "../actions/userAction";
import { userLogout } from "../actions/userAction";

//  initialize userToken from local storage
const accessToken = localStorage.getItem("accessToken")
  ? localStorage.getItem("accessToken")
  : null;

const refreshToken = localStorage.getItem("refreshToken")
  ? localStorage.getItem("refreshToken")
  : null;

// function isExp() {
//   const parseJwt = (token) => {
//     try {
//       return JSON.parse(atob(token.split(".")[1]));
//     } catch (e) {
//       return null;
//     }
//   };

//   var x = parseJwt(accessToken);
//   const timeNow = Date.now();
//   const tokenDate = new Date(0);
//   const expried = tokenDate.setUTCSeconds(x.exp);
//   if (expried - timeNow < 0) {
//     localStorage.removeItem('accessToken');
//     localStorage.removeItem('refreshToken');
//     return true;
//   }
//   else{
//     return false;
//   }

// }

// const exp = isExp();

const initialState = {
  isLoggedIn: false,
  accessToken,
  refreshToken,
  pagination: null,
  error: null,
  message: null,
  status: null,
};

const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  // isLoggedIn: false,
  // accessToken,
  // refreshToken,

  // error: null,
  // message: null,
  // status: null,
  reducer: {
    logout: state => {
      console.log('hello')
      localStorage.removeItem('accessToken')
      localStorage.removeItem('refreshToken')
      state.isLoggedIn = false
      state.accessToken = null
      state.refreshToken = null
      state.status = null
      state.message = null
      state.pagination = null


    }
    
  },

  extraReducers: {
    // login user
    [userLogin.pending]: (state) => {
      state.error = null;
    },
    [userLogin.fulfilled]: (state, { payload }) => {
      console.log(payload.data.status);
      state.accessToken = payload.data.data.accessToken;
      state.refreshToken = payload.data.data.refreshToken;
      state.message = payload.data.message;
      state.status = payload.data.status;
      state.pagination = payload.data.pagination;
      state.isLoggedIn = true;
    },
    [userLogin.rejected]: (state, { payload }) => {
      state.message = payload;
      state.isLoggedIn = false;

      
    },

    [userLogout.fulfilled] : (state,action) => {
      console.log('hello')
      state.isLoggedIn = false
      state.accessToken = null
      state.refreshToken = null
      state.status = null
      state.message = null
      state.pagination = null
    },
    // register user reducer...
  },
});

export const { loginSuccess } = userLogin.fulfilled;
export const {logout}= userLogout.fulfilled;
export default userSlice.reducer;
export const selectLoggedIn = (state) => state.user.isLoggedIn;
export const selectStatus = (state) => state.user.status;
export const selectMessage = (state) => state.user.message

