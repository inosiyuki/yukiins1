import React from 'react'
import Header from '../Header'
import SideBar from '../NavBar_des/SideBar'
import Footer from '../Footer'

export default function ViewProgram() {
  return (
    <div>
      <div >
      {/* Header */}
      <Header />

      {/* Side bar */}

      <div className='flex flex-row w-full'>

        <SideBar />

        <div className='flex-1 h-full'>
          Content here
        </div>


      </div>
      
      
      
      {/* Footer */}


      <Footer />
    </div>
    </div>
  )
}
