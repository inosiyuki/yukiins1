import React from 'react'
import Header from './Header'
import SideBar from './NavBar_des/SideBar'
import Footer from './Footer'

export default function PageNotFound() {
  return (
    <div>
      <div >
      {/* Header */}
      <Header />

      {/* Side bar */}

      <div className='flex flex-row w-full'>

        <SideBar />

        <div className='flex-1 h-full'>
          Page not found 404
        </div>


      </div>
      
      
      
      {/* Footer */}


      <Footer />
    </div>
    </div>
  )
}
