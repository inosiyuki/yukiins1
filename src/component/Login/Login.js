import { useFormik } from "formik";
import React, { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { userLogin } from "../../actions/userAction";
import "./Login.css";
import { selectLoggedIn, selectMessage, selectStatus } from "../../slice/userSlice";


export default function Login() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const isLoggedIn = useSelector(selectLoggedIn)
  const message = useSelector(selectMessage)
  const isLog = useSelector(selectStatus)
  // useEffect(() => {
  //   if (JSON.stringify(status) === '202 ACCEPTED') {
  //     navigate('/dashboard')
  //   }
  // }, [navigate,status])
  
  const formik = useFormik({
    initialValues: {
      email: "",

      password: "",

      isLoggedin: isLoggedIn,
    },
    validationSchema: Yup.object({
      email: Yup.string().email().required("Email is required"),

      password: Yup.string().required("Password is required"),
    }),

    onSubmit: (values) => {
      dispatch(userLogin(values));
           
                    
        //  if(res.status =="202 ACCEPTED"){
        //    navigate('/dashboard')
        // }
        //   else{
        //    navigate('/login')
        //   }
    },
  });
  
  // if(isLoggedIn){
  //   navigate('/dashboard')
  // }
  // else{
  //   navigate('/login')
  // }

  useEffect(() => {
    console.log(isLoggedIn)
   
    if((isLoggedIn)  ){
      navigate('/dashboard')
    }
    else{
      navigate('/login')
    }
  },[navigate, isLoggedIn])

  const handleLoginClick = async (email, password) => {
    const accessToken = null;
    const refreshToken = null;

    //call api
  };

  return (
    <div
      className="background"
      style={{
        width: "100%",
        minHeight: "70vh",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <header
        style={{
          gap: "899px",
          width: "100%",
          height: "79px",
          background: "#0D3B66",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            padding: "10px 60px",
          }}
        >
          <div className="logo_left" style={{ width: "92px", height: "59px" }}>
            <img src={require("./images/logo.png")} />
          </div>

          <div
            className="logo_right"
            style={{
              width: "92px",
              height: "59px",
              display: "flex",
              alignItems: "center",
            }}
          >
            <img src={require("./images/uniGate.png")} />
          </div>
        </div>
      </header>

      <section className="h-screen">
       
        <form
          onSubmit={formik.handleSubmit}
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            position: "absolute",
            padding: "50px 20px",
            gap: "50px",
            minHeight: "511px",
            left: " 53.47%",
            right: "3.89%",
            top: "18.55%",
            bottom: "17.57%",
            background: "#ffffff",
            boxShadow: "0px 20px, 40px rgba(0, 0, 0, 0.16)",
            width: 'max-content',
          }}
        >
          {/* title */}
          <div>
            <h3
              style={{
                width: "487px",
                height: "84px",
                fontFamily: "Roboto",
                fontStyle: "normal",
                fontWeight: "800",
                fontSize: "36px",
                lineHeight: "42px",
                textAlign: "center",
                letterSpacing: "0.2em",
                color: "#000000",
                textShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
              }}
            >
              FPT Fresh Academy Training Management
            </h3>
            <span
              style={{
                fontFamily: "Inter",
                fontStyle: "normal",
                fontWeight: "400",
                fontSize: "16px",
                lineHeight: "19px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              If you don’t have the account, please contact
              <a href="/#" style={{ color: "#ee964b" }}>
                FA.HCM@fsoft.com.vn
              </a>
            </span>
          </div>
          {/* input */}
          <div>
          <div className="pb-1 text-red-500">{message}</div>
            <div>
              
              <input
                placeholder="Enter email"
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  padding: "20px",
                  marginBottom: "20px",
                  gap: "10px",
                  width: "389px",
                  height: "57px",
                  background: "#F1F1F1",
                  borderRadius: "10px",
                }}
                type="email"
                name="email"
                onChange={formik.handleChange}
                value={formik.values.email}
              />
              <div className="text-red-500">{formik.errors.email}</div>
            </div>

            <div>
              <input
                placeholder="Password"
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  padding: "20px",
                  gap: "10px",
                  width: "389px",
                  height: "57px",
                  background: "#F1F1F1",
                  borderRadius: "10px",
                }}
                type="password"
                name="password"
                onChange={formik.handleChange}
                value={formik.values.password}
              />
            </div>
            <div className="text-red-500">{formik.errors.password}</div>
            {/*  */}
            <div
              style={{
                display: "flex",
                justifyContent: "end",
                alignItems: "center",
                width: "100%",
                height: "17px",
                fontFamily: "Inter",
                fontStyle: "italic",
                fontWeight: "400",
                fontSize: "15px",
                lineHeight: "17px",
                color: "#8B8B8B",
              }}
            >
              {/* <span style={{ marginTop: "20px" }}> */}
              <Link style={{ marginTop: "20px" }} to="/forgotpassword">
                Forgot password ?
              </Link>
              {/* </span> */}
            </div>
            <button
              style={{
                width: "389px",
                height: "59px",
                gap: "10px",
                marginTop: "30px",
                padding: "20px",
                background: "#0D3B66",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
                borderRadius: "10px",
                color: "#fff",
              }}
              type="submit"
              // disabled = {loading}
            >
              Sign in
            </button>
          </div>
        </form>
      </section>
      <footer
        className="text-center lg:text-left"
        style={{ backgroundColor: "#0D3B66" }}
      >
        <div
          className="text-center p-5"
          style={{ backgroundColor: "#0D3B66", color: "#fff" }}
        >
          Copyright @2022 BA Warrior. All right reserved
        </div>
      </footer>
    </div>
  );
}

// if (status != "202 ACCEPTED") {
//   navigate("/login");
// } else {

// redirect authenticated user to profile screen
// useEffect(() => {
//   if (localStorage.getItem('accessToken')) {
//     navigatev2('/dashboard')
//   }
//   else{
//     navigatev2('/login')
//   }
// }, [])
// redirect authenticated user to profile screen
// useEffect(() => {
//   if (accessToken) {
//     navigate('/dashboard')
//   }
// }, [navigate,accessToken])
