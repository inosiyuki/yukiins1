
import React, {useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userLogout } from "../../actions/userAction";
// import { useNavigate } from "react-router";
import { logout } from "../../slice/userSlice";
import { selectLoggedIn } from "../../slice/userSlice";

import SideBar from '../NavBar_des/SideBar'
import Footer from '../Footer'
export default function CreateSyllabus() {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(selectLoggedIn);
  const navigate = useNavigate();
  const handleLogOut = (async) => {
    console.log("handleLogOut");
    dispatch(userLogout());
    navigate("/login");
  };
  // const [authenticated, setauthenticated] = useState(null);
  // useEffect(() => {
  //   const loggedInUser = localStorage.getItem('accessToken');
  //   if (loggedInUser) {
  //     setauthenticated(loggedInUser);
  //   }
  // }, []);
  // if(!authenticated){
  //   return <Navigate replace to = '/login'/>
  // }

  useEffect(() => {
    if (!isLoggedIn) {
      if (!("accessToken" in localStorage)) {
        navigate("/login");
        console.log("no token");
      } else {
        console.log("got token");
      }
    } else {
      navigate("/syllabus/view");
    }
  }, [navigate, isLoggedIn]);

  return (
    <div>
      <div>
        {/* Header */}
        <div
      id="header"
      style={{
        width: "100%",
        height: "79px",
        backgroundColor: "#0D3B66",
        position: "sticky",
      }}
    >
      <img
        className="w-28 h-16 flex pt-2.5 pl-10 absolute left-0"
        src={require("../images/logo.png")}
        alt="fpt_logo"
      />

      <div
        className="flex space-x-4"
        style={{
          display: "flex",
          float: "right",
          justifyContent: "space-between",
          padding: "17px 20px 17px",
          margin: "0 auto",
        }}
      >
        <img
          src={require("../images/user_image.jfif")}
          alt="user"
          style={{
            width: "43px",
            height: "42px",
            borderRadius: "50%",
            borderColor: "#c4c4c4",
            borderWidth: "2px",
            borderStyle: "solid",
            float: "right",
          }}
        />
        <div
          className="img_user_text"
          style={{
            float: "right",
            paddingLeft: "7px",
          }}
        >
          <div
            className="user_name"
            style={{
              color: "#ffff",
              fontWeight: "bold",
              flexWrap: "wrap",
            }}
          >
            <span style={{}}>Warror Tran</span>
          </div>
          <div className="logout " style={{ color: "#ffff" }}>
            <button onClick={() => dispatch(handleLogOut())}>Log out</button>
          </div>
        </div>
      </div>

      <div className="logo_uni" style={{}}>
        <img
          src={require("../images/uniGate.png")}
          alt="uni_gate_logo"
          style={{
            width: "107px",
            height: "30px",
            marginTop: "24.5px",
            flexWrap: "wrap",

            display: "inline-flex",
            alignItems: "center",
            float: "right",
          }}
        />
      </div>
    </div>

        {/* Side bar */}

        <div className="flex flex-row w-full h-full bg-yellow-50"
          
        >
          <SideBar />

          <div className="w-full divide-y divide-solid divide-black"
            
          >
            <div className="flex-1   w-full  "
              style={{
                paddingTop: '15px',
                paddingLeft: '15px',
                paddingBottom: '15px',
                fontSize: '24px',
                letterSpacing: '3px'
              }}
            >Syllabus</div>

            <div>Content here</div>
          </div>

          


          
        </div>

        

        {/* Footer */}

        <Footer />
      </div>
    </div>
  );
}
