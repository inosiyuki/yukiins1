import React from 'react'

export default function Footer() {
  return (
    <div
        className="footer"
        style={{
          position: "fixed",
          left: "0px",
          bottom: "0px",
          width: "100%",
          backgroundColor: "#0D3B66",
          height: "70px",
          textAlign: "center",
          display: "flex",
        }}
      >
        <div
          className="footer_text"
          style={{
            font: "Roboto",
            fontWeight: "400",
            fontSize: "18px",
            lineHeight: "21px",
            color: "#ffff",
            verticalAlign: "top",
            margin: "auto",
          }}
        >
          <span>Copyright @2022 BA Warrior. All right reserved</span>
        </div>
      </div>
  )
}
