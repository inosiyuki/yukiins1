
import { NavLink, Outlet } from 'react-router-dom'
import { selectLoggedIn } from '../slice/userSlice'

const ProtectedRoute = () => {
  const { isLoggedIn } = selectLoggedIn

  // show unauthorized screen if no user is found in redux store
  if (isLoggedIn) {
    return (
      <div className='unauthorized'>
        <h1>Unauthorized :(</h1>
        <span>
          <NavLink to='/login'></NavLink>
        </span>
      </div>
    )
  }

  return <Outlet />
}

export default ProtectedRoute
